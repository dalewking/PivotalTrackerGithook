Pivotal Tracker Checkin comment githook
=======================================

This is a hook you can add to a git workspace to easily add story numbers from unfinished stories in the current Pivotal Tracker iteration to your check-in comments (in the format described [here](https://www.pivotaltracker.com/help/api?version=v3#scm_post_commit_message_syntax)).

Coupled with proper SCM hooks to send commits to pivotal, this makes it easy for your check-in comments to appear in the comments for the story and to change the state of the story from the check-in.

This has been tested and works when used on OSX and using MSysGit on Windows. It will probably work on Linux as well if you have xsltproc installed.

When you invoke git commit and it opens the editor the message will include comment lines that look like this in the prepared message: 

    #[Fixes #12345678] This is the story description

Then you simply delete the # from any line that the checkin applies to and if the checkin does not complete the story also delete the word `Fixes` from the line.  

How to use
----------
Copy the `prepare-commit-msg` file to the `.git/hooks` directory of your workspace.

If you are running on Windows also copy msxsl.exe to the `.git/hooks` directory of your workspace.

You need to configure 2 pieces of information from pivotal tracker, your pivotal tracker API token and the numeric identifier of your project. This is done by executing commands like the following within your workspace:

    git config --local pivotal.token a205bc0b43689adef95b9c6f86aa30ce
    git config --local pivotal.project 123456

Those commands will cause a section to be added to your .git/config file like the following. You can alternatively just edit the .git/config directly rather than use the git config commands.

    [pivotal]
    	token = a205bc0b43689adef95b9c6f86aa30ce
    	project = 123456

Note:
-----
Git hook scripts and your .git/config file are local to your workspace and are not part of any commit. If you clone a new workspace you will have to re-copy the githook script and re-configure your pivotal API information in the new workspace.


